import React, { useState } from 'react';
import '../Label/Label.css';

const Counter = () => {
    const [counterValue, setCounterValue] = useState(0);

    let increaseCounterValue = counterValue + 1;
    let decreaseCounterValue = counterValue - 1;
    
    const decreaseCounter = () => {
        if(decreaseCounterValue < 0) {
            alert('Counter value must be positive number');
            return false;
        }
        setCounterValue(decreaseCounterValue);
    }
    
    const increaseCounter = () => {
        setCounterValue(increaseCounterValue);
    }

    return (
        <form>
            <div className='text-center'>
                <button type="button" onClick={decreaseCounter}>-</button>
                <input type="text" className='input-style' name='input_label'  value={counterValue}/>
                <button type="button" onClick={increaseCounter}>+</button>
            </div>
        </form>
    )
}

export default Counter;
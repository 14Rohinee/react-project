import React, {useEffect, useState} from 'react'

export const UseEffect = () => {
    const [name, setName] = useState('Rohinee');
    useEffect(() => {
        console.log('inside');
    }, [])

    const clickMe = () => {
        setName('Rohinee Tandekar');
    }

    console.log('outside');

  return (
    <>
        <h3>{name}</h3>
        <button onClick={clickMe}>Click Me</button>
    </>
  )
}

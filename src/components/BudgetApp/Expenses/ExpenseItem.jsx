import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Expenses.css';
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";


const ExpenseItem = (props) => {
    let id = props.id;
    let month = props.date.getMonth();
    let day = props.date.getDate();
    let year = props.date.getFullYear();
    let amount = props.amount;
    let title = props.title;
    let navigate = useNavigate(); 

    const CreateHandle = () => {
        let path = `Create`; 
        navigate(path);
    }

    return (
       <tbody>
        <tr>
            <td>{id}</td>
            <td>{day}-{month}-{year}</td>
            <td>{amount}</td>
            <td>{title}</td>
            <td>
                <Button as="input" type="button" value="Add" className='m-2' variant='success' onClick={CreateHandle}/>
                <Button as="input" type="button" value="Edit" className='m-2'variant='warning'/>
                <Button as="input" type="button" value="View" className='m-2'variant='info'/>
                <Button as="input" type="button" value="Delete" className='m-2' variant='danger'/>
            </td>
        </tr>
      </tbody>
    )
}

export default ExpenseItem;

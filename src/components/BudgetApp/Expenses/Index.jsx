import React from 'react';
import Table from 'react-bootstrap/Table';
import 'bootstrap/dist/css/bootstrap.min.css';
import ExpenseItem from './ExpenseItem';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const Index = () => {

  const expenses = [
    {
      id: 1,
      date: new Date(2000,7,10),
      title:'Jeans',
      amount: 1000
    },
    {
      id: 2,
      date: new Date(2000,7,10),
      title:'Shirt',
      amount: 2000
    },
    {
      id: 2,
      date: new Date(2000,7,10),
      title:'Court',
      amount: 5000
    }
    
  ];

    return (
      <div>
        <h3 className='text-center p-5'>Expenses CRUD</h3>
        <Container>
          <Row>
            <Col>
            <Table striped bordered hover variant="dark" className='primary'>
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Date</th>
                  <th>Amount</th>
                  <th>Title</th>
                  <th>Action</th>
                </tr>
              </thead>
              <ExpenseItem id = {expenses[0].id} date={expenses[0].date} title={expenses[0].title} amount={expenses[0].amount}/>
              <ExpenseItem id = {expenses[1].id} date={expenses[1].date} title={expenses[1].title} amount={expenses[1].amount}/>
              <ExpenseItem id = {expenses[2].id} date={expenses[2].date} title={expenses[2].title} amount={expenses[2].amount}/>
            </Table>
            </Col>
          </Row>
        </Container>
      </div>
    )
}

export default Index;
import Form from 'react-bootstrap/Form';
import React, { useEffect, useRef } from 'react';
import axios from 'axios';
import { Link, useParams, useNavigate } from 'react-router-dom';
import Button from 'react-bootstrap/Button';

const Edit = () => {
  const { id } = useParams();

  const name = useRef('');
  const username = useRef('');
  const email = useRef('');
  const phone = useRef('');

  const navigate = useNavigate();

  const showUser = () => {

    axios.get(`http://localhost:8000/users/${id}`)
    .then((result) => {  
      name.current.value = result.data.name;
      username.current.value = result.data.username;
      email.current.value = result.data.email;
      phone.current.value = result.data.phone;
    })
    
  }

  useEffect(() => {
    showUser();
  });

  const updateUser = (e) => {
    var data = {
      name: name.current.value,
      username: username.current.value,
      email: email.current.value,
      phone: phone.current.value,
    };

    axios.put(`http://localhost:8000/users/${id}`, data).then(() => {
      setTimeout(() => {
        navigate('/');
      }, 5);
    })

  };

  return (
    <>
        <Form>
          <Form.Group className="mb-3" controlId="name">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" ref={name} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="username">
            <Form.Label>User Name</Form.Label>
            <Form.Control type="text" ref={username} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="email">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" ref={email} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="phone">
            <Form.Label>Phone Number</Form.Label>
            <Form.Control type="text" ref={phone} />
          </Form.Group>
          <Button variant="success" type="button" onClick={updateUser}>
            Submit
          </Button>

          <Link to="/" className=' btn btn-warning'>Cancel</Link>
        </Form>
    </>
  )
}

export default Edit;
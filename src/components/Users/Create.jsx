import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import React, { useState } from 'react';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';


const Create = () => {
  const [name, setName] = useState('');
  const [username, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const navigate = useNavigate();


  const EnteredName = (event) => {
    setName(event.target.value);
  }
  const EnteredUserName = (event) => {
    setUserName(event.target.value);
  }
  const EnteredEmail = (event) => {
    setEmail(event.target.value);
  }
  const EnteredPhone = (event) => {
    setPhone(event.target.value);
  }

  const CreateUser = () => {
    axios.post(`http://localhost:8000/users`, {
      name,
      username,
      email,
      phone,
    }).then(() => {
      setTimeout(() => {
        navigate('/');
      }, 5);
    })
  }

  return (
    <>
        <Form >
          <Form.Group className="mb-3" controlId="name">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" name='name' value={name} onChange={EnteredName}/>
          </Form.Group>

          <Form.Group className="mb-3" controlId="username">
            <Form.Label>User Name</Form.Label>
            <Form.Control type="text" name='user_name' value={username} onChange={EnteredUserName}/>
          </Form.Group>

          <Form.Group className="mb-3" controlId="email">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" name='email' value={email} onChange={EnteredEmail}/>
          </Form.Group>

          <Form.Group className="mb-3" controlId="phone">
            <Form.Label>Phone Number</Form.Label>
            <Form.Control type="text" name='email' value={phone} onChange={EnteredPhone}/>
          </Form.Group>

          <Button variant="success" type="button" onClick={CreateUser}>
            Submit
          </Button>
          <Link to="/" className=' btn btn-warning m-5'>Cancel</Link>

        </Form>
    </>
  )
}

export default Create;
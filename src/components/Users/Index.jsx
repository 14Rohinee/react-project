import axios from 'axios';
import { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.min.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Show from './Show';
import Swal from "sweetalert2";
import './users.css';
import { BsSearch } from "react-icons/bs";


const Index = () => 
{
    const [users, setUsers] = useState([]);
    const [show, setShow] = useState(false);
    const [userId, setUserId] = useState('');
    const [search, setSearch] = useState([]);

    const loadUsers = async () => {
        let result = await axios.get("http://localhost:8000/users");
        setUsers(result.data);
    }

    const DeleteItem = (id) => {
        Swal.fire({
            title: 'Do you want to delete this user?',
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: 'Yes',
          }).then((result) => {
            if (result.isConfirmed) {
                axios.delete(`http://localhost:8000/users/${id}`)  
                .then(() => {  
                    const user = users.filter(item => item.id !== id);  
                    setUsers(user);
                })    
                Swal.fire('User Deleted SuccessFully', '', 'success')
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    }

    const handleShow = id => {  
        setUserId(id);
        setShow(true);
    };

    const handleClose = () => setShow(false);
    const navigate = useNavigate();
    
    useEffect(() => {
        loadUsers();
    }, []);

    const searchUser = async (e) => {
        let search = e.target.value.toLowerCase();

        if(search.length > 0){
            console.log('a');
            let result = users.filter((user) => user.name.toLowerCase().includes(search));

            if(result.length > 0){
                setSearch(result)
            }

        }
        else {
            loadUsers();
        }

        // (search !== '' && result.length > 0) ? setUsers(result) : setUsers(users);
    }

    return (
        <>
            <div>
                <Container>
                    <Row>
                        <Col>
                            <h3 className='text-center p-5'>Expenses CRUD</h3>
                        </Col>
                        <Col>
                            <Link to="/create" className='btn btn-success px-5 text-center' id='add-user'>Add</Link>
                        </Col>
                        <Col>
                            <input type="text" name="search" id="search" placeholder='Search for user' onKeyUp={ searchUser }/><BsSearch id='button'/>
                        </Col>
                        <Col>
                            <button type="button" className='btn btn-primary' id='reset'>Reset</button>
                        </Col>
                    </Row>
                </Container>
                
                <Container>
                    <Row>
                        <Col>
                            <Table striped bordered hover variant="dark" className='primary'>
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>UserName</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {users.map((user) => {
                                        const { id, name, username, email, phone} = user;
                                        
                                        return (
                                            <tr key={ user.id }>
                                                <td>{id}</td>
                                                <td>{name}</td>
                                                <td>{username}</td>
                                                <td>{email}</td>
                                                <td>{phone}</td>
                                                <td>
                                                    
                                                    <Button variant="primary" onClick={() => handleShow(user.id) } >View</Button>
                                                    {/* <Show show={show} onClose={handleClose} id={ user.id }/> */}

                                                    {/* <Button variant="primary" onClick={ handleShow } >View</Button> */}
                                                    <Button variant="warning" className='m-2' onClick={() => navigate(`/edit/${user.id}`)} >Edit</Button>
                                                    <Button as="input" type="button" value="Delete" className='m-2' variant='danger' onClick={(e) => DeleteItem(user.id)}/>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>                                
                            </Table>
                        </Col>
                    </Row>
                </Container>
            </div>
            <Show onClose ={handleClose} show={show} id={userId} />
        </>
    )
}

export default Index;

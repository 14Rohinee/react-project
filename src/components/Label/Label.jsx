import React, { useState } from 'react';
import './Label.css';

const Label = () => {
    const [inputLabel, setInputLabel] = useState('');

    const changeLabel = (event) => {
        setInputLabel(event.target.value);
    }

    return(
        <form>
            <div className='text-center'>
                <label for="input_label"> Input Label</label>
                <input type="text" className='input-style' name='input_label' onChange={changeLabel} />
                <br/>
                <label for="output_label"> Output Label</label>
                <input type="text" className='input-style' name='output_label' value={inputLabel}/>          
            </div>
        </form>     
    )
}

export default Label;
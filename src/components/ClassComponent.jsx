import React, { Component } from 'react';

class ClassComponent extends Component {
    constructor() {
        super();
        this.state = {name:'Rohinee'};

        setTimeout(() => {
            this.setState({name:'Rohinee Tandekar'});
        }, 2000);
    }

    render(){
        return (
            <div>{this.state.name}</div>
        );
    }
}

export default ClassComponent;
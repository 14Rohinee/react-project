import { Routes ,Route } from 'react-router-dom';
import Index from './components/Users/Index';
import Create from './components/Users/Create';
import Show from './components/Users/Show';
import Edit from './components/Users/Edit';
// import Index from './components/Expenses/Index';
// import ClassComponent from './components/ClassComponent';
// import { UseEffect } from './components/UseEffect';



const App = () => { 
  return (
    <>
    <Routes>
      <Route path='/' element={ <Index /> } />
      <Route path='/create' element={ <Create /> } />
      <Route path='/show/:id' element={ <Show /> } />
      <Route path='/edit/:id' element={ <Edit /> } />
      {/* <Route path='*' element={ <Error /> } />` */}
    </Routes>
      {/* <ClassComponent/> */}
      {/* <UseEffect/> */}
    </>
  )

  // return (
  //   <>
  //     <Index />
  //   </>
  // )

}

export default App;